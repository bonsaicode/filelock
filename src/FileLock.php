<?php
namespace BonsaiCode;
/**
 * FileLock
 *
 * Used to prevent a script from being run more than once at the same time by providing mutex functionality via locking a file on the local machine.
 *
 * Scenario:
 *	1. You run php script A and it acquires the lock having a given name, e.g. "lockA"
 *    2. You try to run script A again while it is still running.  It tries to acquire the same "lockA", but cannot because the first script has the lock.
 *    3. After the first script A ends, the "lockA" is released, and then you could run the script again.
 *
 * When a lock is released, the underlying lock file is removed.
 * If the script dies unexpectedly, the lockfile remains but can be relocked by the next script.
 *
 * This class is intended for single server environments or when you know the process will only run on one server.
 *
 * DO NOT use this if you are running in a multi-server environment where duplicate VMs could be dynamically created, e.g. auto scaling environment.
 * Otherwise the same process could run at the same time on multiple servers.
 * Therefore we do not use sys_get_temp_dir() for the lockfile directory because the server could change between requests.
 * If you are running in a multi-server environment with dynamically created VMs, please use the BonsaiCode\Redis::lockAcquire and BonsaiCode\Redis::lockRelease methods (with a redis service)
 *
 * Note: This does not work on Windows localhost w/NGINX:
 * 1. the script will wait until any lockfile is released instead of the intended named lockfile.
 * 2. two scripts won't run at same time, but one will wait for the other instead of exiting
 *
 * Dependency:
 *
 *	This class assumes a LOCK_DIR constant is defined with a value of the directory in which to create the lock file, e.g.:
 *
 *		const LOCK_DIR = '/var/www/mysite.com/locks';
 *
 *	LOCK_DIR should be located outside of any web/code directories so that the code can be updated without affecting any existing lock files.
 *	LOCK_DIR MUST BE WRITABLE BY NGINX.
 *
 * @author Alan Davis <alan@bonsaicode.dev>
 * @copyright 2018 BonsaiCode, LLC
 *
 */
class FileLock {

	/**
	 *
	 * @var string $lockfile The name of the lockfile.
	 * @var resource $fh Filehandle for the lockfile.
	 */
	private $lockfile = null;
	private $fh       = null;

	/**
	 * Instantiation an object and defined the name of the lock.
	 * LOCK_DIR constant must be defined WITHOUT a trailing directory separating slash.
	 * LOCK_DIR directory MUST by writable by your web server, e.g. chmod 775
	 *
	 * @param string $lockname The name of the lock.  This allows you to have multiple locks for different processes running at the same time.
	 */
	function __construct( string $lockname ) {
		$this->lockfile = LOCK_DIR.DIRECTORY_SEPARATOR.$lockname.'.lock';
	}

	/**
	 * Acquire the lock.
	 *
	 * @return int  1 = lock acquired
	 *             -1 = lock is already taken (by another process)
	 *             -2 = the lockfile cannot be opened, most likely due to permissions on the LOCK_DIR directory
	 *
	 */
	function acquire() : int {
		$status   = 0;
		$this->fh = fopen( $this->lockfile, 'w' );
		if( $this->fh !== false ) { # lockfile is open
			# use LOCK_NB to return immediately if lock cannot be acquired
			if( flock( $this->fh, LOCK_EX | LOCK_NB ) === false ) { # cannot acquire lock
				fclose( $this->fh );
				$status = -1; # file already locked (by another process)
			} else { # we have acquired a lock on the file
				$status = 1;
			}
		} else { # cannot open lockfile, permission problem?
			$status = -2;
		}
		return $status;
	}

	/**
	 * Release the lock.  The underlying lockfile is removed.
	 *
	 */
	function release() : void {
		if( $this->fh ) {
			flock( $this->fh, LOCK_UN );
			fclose( $this->fh );
			if( file_exists( $this->lockfile ) ) {
				unlink( $this->lockfile );
			}
		}
	}
}