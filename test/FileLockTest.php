<?php
exit;

require_once $_SERVER['APP_CONFIG'];
ini_set( 'display_errors', 1 );
header( "Content-Type: text/plain" );

$lock       = new BonsaiCode\FileLock( 'MyLock' );
$lockStatus = $lock->acquire();
if( $lockStatus === -1 ) {
	echo "Script already running\n";
	exit();
} else if( $lockStatus === -2 ) {
	echo "Cannot open lock, permission problem?\n";
	exit();
} else {
	echo 'Lock acquired at '.date( 'Y-m-d H:i:s' )."\n";
	# keep the lock for 5 seconds
	# this will give you time to try running this same script in another browser tab to confirm that you cannot acquire the lock again until the first lock is released 5 seconds later
	sleep( 5 );
}

$lock->release();
echo 'Lock released at '.date( 'Y-m-d H:i:s' )."\n";